package com.example.trabajo.monetizationexample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var rewardAd: RewardedVideoAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Admob initialization
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713")


        //BannerAd
        val adRequest = AdRequest.Builder().build()
        bannerAd.loadAd(adRequest)

        //Interstitial Ad
        val interstitialAd = InterstitialAd(this)
        interstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        interstitialAd.loadAd(AdRequest.Builder().build())
        showInterstitialAd.setOnClickListener {
            interstitialAd.show()
        }


        //RewardVideoAd
        rewardAd = MobileAds.getRewardedVideoAdInstance(this)
        rewardAd.rewardedVideoAdListener = object : RewardedVideoAdListener {
            override fun onRewardedVideoAdClosed() {}

            override fun onRewardedVideoAdLeftApplication() {}

            override fun onRewardedVideoAdLoaded() {}

            override fun onRewardedVideoAdOpened() {}

            override fun onRewardedVideoCompleted() {}

            override fun onRewarded(p0: RewardItem?) {}

            override fun onRewardedVideoStarted() {}

            override fun onRewardedVideoAdFailedToLoad(p0: Int) {}
        }

        rewardAd.loadAd("ca-app-pub-3940256099942544/5224354917", AdRequest.Builder().build())
        rewardVideoText.setOnClickListener {
            if (rewardAd.isLoaded) {
                rewardAd.show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        rewardAd.resume(this)
    }

    override fun onPause() {
        super.onPause()
        rewardAd.pause(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        rewardAd.destroy(this)
    }
}
